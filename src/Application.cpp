#include "stdafx.h"
#include "Application.h"

Application::Application()
{
    cameraZ = -5;
    cameraAngle = glm::vec2(0.1f, 0.1f);
    drag = false;
    capture = true;
}

Application::~Application()
{
}

void Application::Init(int width, int height)
{
    kinect.InitializeDefaultSensor();
    Resize(width, height);
    glClearColor(.3f, .3f, .3f, 1.f);
    glEnable(GL_DEPTH_TEST);
}

void Application::Render()
{
    static int lastSecond = -1;
    auto t = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = t - timer;
    if (timerStarted)
    {
        if (diff.count() > 3.0)
        {
            timerStarted = false;
            capture = false;
        }
        int s = static_cast<int>(diff.count());
        if (lastSecond != s)
        {
            Beep(1000, s < 3 ? 100 : 500);
            lastSecond = s;
        }
    }

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (capture)
        kinect.Update();

    glm::mat4 proj = glm::perspective(70.f, aspect, .1f, 1000.f); //glm::ortho<float>(-aspect, aspect, -1, 1, -10, 10);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMultMatrixf(glm::value_ptr(proj));

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    float speed = std::fabs(cameraZ) * 0.3f;
    glm::mat4 camera = glm::translate(glm::vec3(0, 0, cameraZ * speed)) * glm::translate(glm::vec3(cameraPan * speed, 0)) *
        glm::eulerAngleXY(cameraAngle.x, cameraAngle.y);
    glMultMatrixf(glm::value_ptr(camera));

    /*
    DrawGrid(10, 1);
    DrawFrame(glm::vec3(), glm::quat(), 1);

    KinJoint root("Root");
    KinJoint A("Child1");
    KinJoint B("Child2");

    static float angle = 0;
    angle += 2.1f;

    root.children.push_back(&A);
    A.children.push_back(&B);
    A.parent = &root;
    B.parent = &A;
    root.rawPos = glm::vec3(0, 1, 0);
    root.rawRot = glm::angleAxis(glm::radians(angle), glm::vec3(0, 0, 1));
    A.rawPos = glm::vec3(1, 1, 0);
    B.rawPos = glm::vec3(2, 0, 0);

    std::vector<KinJoint*> joints;
    joints.push_back(&root);
    joints.push_back(&A);
    joints.push_back(&B);

    for each (auto j in joints)
    {
        j->m = glm::translate(j->rawPos) * glm::mat4_cast(j->rawRot);
    }

    for each (auto j in joints)
    {
        glm::mat4 m;
        KinJoint* p = j->parent;
        while (p)
        {
            m = p->m * m;
            p = p->parent;
        }
        j->pos = glm::vec3(m * glm::vec4(j->rawPos, 1));
    }

    glLineWidth(2);
    glPointSize(5);
    glColor3f(1, 1, 1);
    for each (auto j in joints)
    {
        DrawPoint(j->pos);
        if (j->parent)
        {
            DrawLine(j->parent->pos, j->pos);
        }
    }

    return;
    */

    //glm::vec3 p0(0, 0, 0);
    //glm::vec3 p1(-0.001, 1, 0);
    //glm::vec3 y = glm::normalize(p1 - p0);
    //glm::vec3 x = glm::normalize(glm::cross(y, glm::vec3(0, 1, 0)));
    //glm::vec3 z = glm::normalize(glm::cross(x, y));
    //glm::quat q = glm::normalize(glm::quat_cast(glm::mat3(x, y, z)));
    //glPointSize(5);
    //glColor3f(1, 1, 1);
    //DrawPoint(p0);
    //DrawPoint(p1);
    //DrawLine(p0, p1);
    //DrawLine(p0, q * glm::vec3(1, 0, 0));
    //DrawLine(p0, q * glm::vec3(0, 1, 0));
    //DrawLine(p0, q * glm::vec3(0, 0, 1));
    //glLineWidth(3);
    //DrawFrame(glm::vec3(), q, 3);
    //return;

    //static float angle = 0;
    //angle += 0.001f;
    //glm::quat rot = glm::angleAxis(glm::radians(angle), glm::vec3(0, 1, 0));


    //glm::quat at = glm::quat_cast(glm::mat3(glm::lookAt(glm::vec3(0, 1, 0), glm::vec3(0), glm::vec3(0, 0, 1))));
    //glm::quat q = glm::quat_cast(glm::mat3(glm::lookAt(glm::vec3(), glm::vec3(1, 1, 0), glm::vec3(0, 1, 0))));

    //drawFrame(glm::vec3(-5, 0, 0), q, 3);
    //drawFrame(glm::vec3(0, 0, 0), glm::inverse(q)*at*q, 3);
    //drawFrame(glm::vec3(5, 0, 0), at, 3);

    //return;

    // Draw main axis
    glLineWidth(3);
    DrawFrame(glm::vec3(), glm::quat(), 3);

    // Draw the joints
    glPointSize(10);
    glColor3f(1, 1, 1);
    for each (auto j in kinect.m_jointMap)
    {
        DrawPoint(j.second.pos);
    }

    // Draw the bones
    glLineWidth(3);
    glBegin(GL_LINES);
    for each (auto j in kinect.m_jointMap)
    {
        int parentID = kinect.m_parentMap[j.first];
        if (parentID == -1)
            continue;
        glm::vec3 pp = kinect.m_jointMap[(JointType)parentID].pos;
        glm::vec3 p = j.second.pos;
        glVertex3f(p.x, p.y, p.z);
        glVertex3f(pp.x, pp.y, pp.z);
    }
    glEnd();

    // Draw joints axis
    glLineWidth(2);
    for each (auto j in kinect.m_jointMap)
    {
        glm::quat q = kinect.m_jointMap[kinect.m_orientMap[j.first]].rot;
        glm::vec3 p = j.second.pos;
        DrawFrame(p, q, 0.1f);
    }
}

void Application::Terminate()
{

}

void Application::Resize(int width, int height)
{
    this->width = width;
    this->height = height;
    aspect = (float)width / (float)height;
}

void Application::DrawGrid(int w, float l)
{
    glLineWidth(1);
    glColor3f(.1f, .1f, .1f);
    glBegin(GL_LINES);
    for (int i = -w; i <= w; i++)
    {
        glVertex3f(-w*l, 0, l*i);
        glVertex3f(w*l, 0, l*i);
        glVertex3f(i*l, 0, -w*l);
        glVertex3f(i*l, 0, w*l);
    }
    glEnd();
}

void Application::DrawFrame(glm::vec3 p, glm::quat q, float l)
{
    glPushMatrix();
    glTranslatef(p.x, p.y, p.z);

    glm::vec3 x = q * glm::vec3(l, 0, 0);
    glm::vec3 y = q * glm::vec3(0, l, 0);
    glm::vec3 z = q * glm::vec3(0, 0, l);

    glBegin(GL_LINES);

    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(x.x, x.y, x.z);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(y.x, y.y, y.z);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(z.x, z.y, z.z);

    glEnd();
    glPopMatrix();
}

void Application::DrawPoint(glm::vec3 p)
{
    glPushMatrix();
    glTranslatef(p.x, p.y, p.z);
    glBegin(GL_POINTS);
    glVertex3f(0, 0, 0);
    glEnd();
    glPopMatrix();
}

void Application::DrawLine(glm::vec3 p0, glm::vec3 p1)
{
    glBegin(GL_LINES);
    glVertex3f(p0.x, p0.y, p0.z);
    glVertex3f(p1.x, p1.y, p1.z);
    glEnd();
}

void Application::MouseMove(double x, double y)
{
    static double oldx = x;
    static double oldy = y;
    // Left mouse rotate
    if (drag & 1)
    {
        cameraAngle.y += (float)((x - oldx) * 0.01);
        cameraAngle.x += (float)((y - oldy) * 0.01);
    }
    // Right mouse pan
    if (drag & 2)
    {
        cameraPan.x += (float)((x - oldx) * 0.01);
        cameraPan.y -= (float)((y - oldy) * 0.01);
    }
    oldx = x;
    oldy = y;

}

void Application::MouseDown(int button)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) drag |= 1;
    if (button == GLFW_MOUSE_BUTTON_RIGHT) drag |= 2;
    if (button == GLFW_MOUSE_BUTTON_MIDDLE) drag |= 4;
}

void Application::MouseScroll(double x, double y)
{
    cameraZ += (float)(y * 0.1);
}

void Application::MouseUp(int button)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) drag &= ~1;
    if (button == GLFW_MOUSE_BUTTON_RIGHT) drag &= ~2;
    if (button == GLFW_MOUSE_BUTTON_MIDDLE) drag &= ~4;
}

void Application::KeyDown(int key)
{
    switch (key)
    {
    case GLFW_KEY_SPACE:
        timer = std::chrono::monotonic_clock::now();
        timerStarted = true;
        capture = true;
        break;
    }
}
