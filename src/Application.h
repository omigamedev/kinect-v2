#pragma once
#include "KinectSensor.h"

class Application
{
    KinectSensor kinect;
    std::chrono::time_point<std::chrono::system_clock> timer;
    bool capture;
    bool timerStarted;
    int width;
    int height;
    float aspect;
    glm::vec2 cameraAngle;
    glm::vec2 cameraPan;
    float cameraZ;
    int drag; // 0:none, 1:left, 2:right, 4:middle
public:
    Application();
    ~Application();

    void Init(int width, int height);
    void Render();
    void DrawGrid(int w, float l);
    void DrawFrame(glm::vec3 p, glm::quat q, float l);
    void DrawPoint(glm::vec3 p);
    void DrawLine(glm::vec3 p0, glm::vec3 p1);
    void Resize(int width, int height);
    void Terminate();
    void MouseMove(double x, double y);
    void MouseDown(int button);
    void MouseUp(int button);
    void MouseScroll(double x, double y);
    void KeyDown(int key);
};

