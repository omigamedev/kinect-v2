#include "stdafx.h"
#include "KinectSensor.h"


KinectSensor::KinectSensor()
{
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_SpineBase,     -1));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_SpineMid,      JointType_SpineBase));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_SpineShoulder, JointType_SpineMid));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_Neck,          JointType_SpineShoulder));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_Head,          JointType_Neck));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_ShoulderLeft,  JointType_SpineShoulder));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_ElbowLeft,     JointType_ShoulderLeft));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_WristLeft,     JointType_ElbowLeft));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_HandLeft,      JointType_WristLeft));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_ShoulderRight, JointType_SpineShoulder));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_ElbowRight,    JointType_ShoulderRight));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_WristRight,    JointType_ElbowRight));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_HandRight,     JointType_WristRight));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_HipLeft,       JointType_SpineBase));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_KneeLeft,      JointType_HipLeft));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_AnkleLeft,     JointType_KneeLeft));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_FootLeft,      JointType_AnkleLeft));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_HipRight,      JointType_SpineBase));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_KneeRight,     JointType_HipRight));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_AnkleRight,    JointType_KneeRight));
    m_parentMap.insert(std::make_pair<JointType, int>(JointType_FootRight,     JointType_AnkleRight));

    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_SpineBase,     KinJoint("SpineBase")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_SpineMid,      KinJoint("SpineMid")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_SpineShoulder, KinJoint("SpineShoulder")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_Neck,          KinJoint("Neck")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_Head,          KinJoint("Head")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_ShoulderLeft,  KinJoint("ShoulderLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_ElbowLeft,     KinJoint("ElbowLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_WristLeft,     KinJoint("WristLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_HandLeft,      KinJoint("HandLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_ShoulderRight, KinJoint("ShoulderRight")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_ElbowRight,    KinJoint("ElbowRight")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_WristRight,    KinJoint("WristRight")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_HandRight,     KinJoint("HandRight")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_HipLeft,       KinJoint("HipLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_KneeLeft,      KinJoint("KneeLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_AnkleLeft,     KinJoint("AnkleLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_FootLeft,      KinJoint("FootLeft")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_HipRight,      KinJoint("HipRight")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_KneeRight,     KinJoint("KneeRight")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_AnkleRight,    KinJoint("AnkleRight")));
    m_jointMap.insert(std::make_pair<JointType, KinJoint>(JointType_FootRight,     KinJoint("FootRight")));

    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_SpineBase,     JointType_SpineBase));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_SpineMid,      JointType_SpineMid));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_SpineShoulder, JointType_SpineShoulder));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_Neck,          JointType_Neck));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_Head,          JointType_Head));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_ShoulderLeft,  JointType_ElbowLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_ElbowLeft,     JointType_WristLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_WristLeft,     JointType_HandLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_HandLeft,      JointType_HandLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_ShoulderRight, JointType_ElbowRight));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_ElbowRight,    JointType_WristRight));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_WristRight,    JointType_HandRight));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_HandRight,     JointType_HandRight));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_HipLeft,       JointType_KneeLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_KneeLeft,      JointType_AnkleLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_AnkleLeft,     JointType_FootLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_FootLeft,      JointType_FootLeft));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_HipRight,      JointType_KneeRight));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_KneeRight,     JointType_AnkleRight));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_AnkleRight,    JointType_FootRight));
    m_orientMap.insert(std::make_pair<JointType, JointType>(JointType_FootRight,     JointType_FootRight));

    for each (auto j in m_jointMap)
    {
        for each (auto other in m_jointMap)
        {
            if (other.first == m_parentMap[j.first])
            {
                j.second.parent = &other.second;
                other.second.children.push_back(&j.second);
            }
        }
    }
}

KinectSensor::~KinectSensor()
{
}

HRESULT KinectSensor::InitializeDefaultSensor()
{
    HRESULT hr;

    hr = GetDefaultKinectSensor(&m_pKinectSensor);
    if (FAILED(hr))
    {
        return hr;
    }

    if (m_pKinectSensor)
    {
        // Initialize the Kinect and get coordinate mapper and the body reader
        IBodyFrameSource* pBodyFrameSource = NULL;

        hr = m_pKinectSensor->Open();

        if (SUCCEEDED(hr))
        {
            hr = m_pKinectSensor->get_BodyFrameSource(&pBodyFrameSource);
        }

        if (SUCCEEDED(hr))
        {
            hr = pBodyFrameSource->OpenReader(&m_pBodyFrameReader);
        }

        SafeRelease(pBodyFrameSource);
    }

    if (!m_pKinectSensor || FAILED(hr))
    {
        return E_FAIL;
    }

    return hr;
}

void KinectSensor::Update()
{
    static float angle = 0;
    angle += 0.1f;
    if (!m_pBodyFrameReader)
    {
        return;
    }

    IBodyFrame* pBodyFrame = NULL;
    HRESULT hr = m_pBodyFrameReader->AcquireLatestFrame(&pBodyFrame);
    if (!SUCCEEDED(hr))
        return;

    INT64 nTime = 0;
    hr = pBodyFrame->get_RelativeTime(&nTime);
    if (!SUCCEEDED(hr))
    {
        SafeRelease(pBodyFrame);
        return;
    }


    IBody* ppBodies[BODY_COUNT] = { 0 };
    hr = pBodyFrame->GetAndRefreshBodyData(_countof(ppBodies), ppBodies);
    if (SUCCEEDED(hr) )
    {
        for (int i = 0; i < BODY_COUNT; ++i)
        {
            IBody* pBody = ppBodies[i];
            if (pBody)
            {
                BOOLEAN bTracked = false;
                hr = pBody->get_IsTracked(&bTracked);

                if (SUCCEEDED(hr) && bTracked)
                {
                    Joint joints[JointType_Count];
                    JointOrientation orients[JointType_Count];

                    hr = pBody->GetJoints(_countof(joints), joints);
                    hr = pBody->GetJointOrientations(_countof(orients), orients);
                    if (SUCCEEDED(hr))
                    {
                        /*
                        // Read raw values
                        for each (auto j in m_jointMap)
                        {
                            j.second.rawPos = Kinect2glm(joints[j.first].Position);
                            j.second.rawRot = Kinect2glm(orients[m_orientMap[j.first]].Orientation);
                        }

                        // Compute the local transformation
                        for each (auto j in m_jointMap)
                        {
                            j.second.m = glm::translate(j.second.pos) * glm::mat4_cast(j.second.rot);
                        }

                        // Global -> Local
                        for each (auto j in m_jointMap)
                        {
                            glm::mat4 m;
                            KinJoint* p = j.second.parent;
                            while (p)
                            {
                                m = m * p->m;
                                p = p->parent;
                            }
                            glm::vec3 p = glm::vec3(glm::inverse(m) * glm::vec4(j.second.rawPos, 1));
                            j.second.vel = p - j.second.pos;
                            j.second.pos = p;
                        }
                        */

                        for (int i = 0; i < JointType_Count; i++)
                        {
                            JointType id = (JointType)i;
                            if(m_jointMap.count(id) == 0)
                                continue;

                            glm::vec3 up(0, 1, 0);
                            glm::vec3 pos = Kinect2glm(joints[i].Position);
                            //if (glm::length2(m_jointMap[id].pos) > 0)
                            //    up = pos - m_jointMap[id].pos;
                            m_jointMap[id].pos = pos;

                            if (m_parentMap[id] != -1)
                            {
                                int pid = m_parentMap[id];
                                glm::vec3 p0 = Kinect2glm(joints[pid].Position);
                                glm::vec3 p1 = Kinect2glm(joints[i].Position);
                                glm::vec3 y = glm::normalize(p1 - p0);
                              
                                glm::vec3 crossx = glm::cross(y, up);
                                glm::vec3 x = glm::normalize(crossx);
                                glm::vec3 z = glm::normalize(glm::cross(x, y));

                                glm::quat q = glm::normalize(glm::quat_cast(glm::mat3(x, y, z)));
                                m_jointMap[id].rot = q;
                            }

                        }
                    }
                }
            }
            SafeRelease(pBody);
        }
    }

    SafeRelease(pBodyFrame);
}

glm::vec3 KinectSensor::Kinect2glm(CameraSpacePoint &p)
{
    return glm::vec3(p.X, p.Y, p.Z);
}

glm::quat KinectSensor::Kinect2glm(Vector4 &q)
{
    return glm::quat(q.w, q.x, q.y, q.z);
}
