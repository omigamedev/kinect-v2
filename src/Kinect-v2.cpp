#include "stdafx.h"
#include "Application.h"

Application app;
bool active;

void window_resize(GLFWwindow* window, int w, int h)
{
    app.Resize(w, h);
}

void window_key(GLFWwindow* window,
    int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        switch (key)
        {
        case GLFW_KEY_ESCAPE:
            active = false;
            break;
        default:
            break;
        }
    }
    app.KeyDown(key);
}

void window_cursor(GLFWwindow* window, double x, double y)
{
    app.MouseMove(x, y);
}

void window_button(GLFWwindow* window, int button, int action, int mods)
{
    switch (action)
    {
    case GLFW_PRESS:
        app.MouseDown(button);
        break;
    case GLFW_RELEASE:
        app.MouseUp(button);
        break;
    }
}

void window_scroll(GLFWwindow*, double x, double y)
{
    app.MouseScroll(x, y);
}

int _tmain(int argc, _TCHAR* argv[])
{
    glfwInit();
    GLFWwindow* window = glfwCreateWindow(800, 600, "Kinect Windows v2", nullptr, nullptr);
    glfwSetWindowSizeCallback(window, window_resize);
    glfwSetKeyCallback(window, window_key);
    glfwSetCursorPosCallback(window, window_cursor);
    glfwSetMouseButtonCallback(window, window_button);
    glfwSetScrollCallback(window, window_scroll);
    glfwMakeContextCurrent(window);
    glewInit();
    app.Init(800, 600);
    active = true;
    while (!glfwWindowShouldClose(window) && active)
    {
        app.Render();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    app.Terminate();
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

