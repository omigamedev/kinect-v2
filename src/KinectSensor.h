#pragma once

// Safe release for interfaces
template<class Interface>
inline void SafeRelease(Interface *& pInterfaceToRelease)
{
    if (pInterfaceToRelease != NULL)
    {
        pInterfaceToRelease->Release();
        pInterfaceToRelease = NULL;
    }
}

struct KinJoint
{
    KinJoint* parent;
    std::vector<KinJoint*> children;
    std::string name;

    // Last read values
    glm::quat rawRot;
    glm::vec3 rawPos;
    
    // Relative to parent
    glm::vec3 pos;
    glm::quat rot;

    glm::mat4 m;
    glm::vec3 vel;
    
    KinJoint() : parent(nullptr), pos(0,1,0), vel(0,1,0) { }
    KinJoint(std::string name) : name(name), parent(nullptr), pos(0,1,0), vel(0,1,0) { }
};

class KinectSensor
{
    IKinectSensor*     m_pKinectSensor;
    IBodyFrameReader*  m_pBodyFrameReader;
public:
    std::map<JointType, int> m_parentMap;
    std::map<JointType, KinJoint> m_jointMap;
    std::map<JointType, JointType> m_orientMap;
    
    KinectSensor();
    ~KinectSensor();

    HRESULT InitializeDefaultSensor();
    void Update();
    glm::vec3 Kinect2glm(CameraSpacePoint &p);
    glm::quat Kinect2glm(Vector4 &q);
};

